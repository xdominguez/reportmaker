using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PuppeteerSharp;
using PuppeteerSharp.Media;

namespace ReportMakerWeb.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
        private static string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        [HttpGet("[action]")]
        public IEnumerable<WeatherForecast> WeatherForecasts()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                DateFormatted = DateTime.Now.AddDays(index).ToString("d"),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            });
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> CreateReport()
        {
            var browser = await Puppeteer.LaunchAsync(new LaunchOptions { Headless = true, ExecutablePath = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"});
            using(var page = await browser.NewPageAsync())
            {
                var rng = new Random();
                var list = Enumerable.Range(1, 50).Select(index => new WeatherForecast
                {
                    DateFormatted = DateTime.Now.AddDays(index).ToString("d"),
                    TemperatureC = rng.Next(-20, 55),
                    Summary = Summaries[rng.Next(Summaries.Length)]
                });
                var thStyle = "style='border-bottom: 2px solid #dee2e6;'";
                var rows = list.Aggregate("", (current, data) => current + $"<tr><td>{data.DateFormatted}</td><td>{data.TemperatureC}</td><td>{data.TemperatureF}</td><td>{data.Summary}</td></tr>");
                var html = $"<div style='font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto, \"Helvetica Neue\", Arial, \"Noto Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\";'><table style='width:100%'><thead style='text-align:left'><th {thStyle}>Date</th><th {thStyle}>Temp. (C)</th><th {thStyle}>Temp. (F)</th><th {thStyle}>Summary</th></thead>" +
                           $"<tbody>{rows}</tbody></table></div>";
                await page.SetContentAsync(html);
                var pdf = await page.PdfDataAsync(new PdfOptions()
                {
                    DisplayHeaderFooter= true,
                    HeaderTemplate= "<div style='font-size:10px; margin:0 20px;width:100%'><div style='text-align:right;'><span class='pageNumber'></span> of <span class='totalPages'></span></div></div>",
                    FooterTemplate= "<div style='font-size:10px; margin:0 20px;width:100%'><div style='text-align:right;'><span class='pageNumber'></span> of <span class='totalPages'></span></div></div>",
                    MarginOptions = new MarginOptions()
                    {
                        Bottom = "50px",
                        Left = "20px",
                        Right = "20px",
                        Top = "50px"
                    },
                    Format= PaperFormat.A4
                });
                var content = new System.IO.MemoryStream(pdf);
                var contentType = "application/pdf";
                var fileName = "report.pdf";
                return File(content, contentType, fileName);
            }
        }

        public class WeatherForecast
        {
            public string DateFormatted { get; set; }
            public int TemperatureC { get; set; }
            public string Summary { get; set; }

            public int TemperatureF
            {
                get { return 32 + (int) (TemperatureC / 0.5556); }
            }
        }
    }
}